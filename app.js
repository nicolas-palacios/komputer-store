const balanceElement = document.getElementById("balance");
const computersElement = document.getElementById("computers");
const salaryElement = document.getElementById("salary");
const workElement = document.getElementById("work-button");
const saveSalaryElement = document.getElementById("save-button");
const loanElement = document.getElementById("loan");
const loanButtonElement = document.getElementById("loan-button");
const hiddenLoanButton = (document.getElementById(
  "repay-loan-button"
).style.display = "none");
const buyButtonElement = document.getElementById("buy-button");
const repayLoanButtonElement = document.getElementById("repay-loan-button");
const computerPriceElement = document.getElementById("computer-price");
const computerSpecsElement = document.getElementById("computer-specs");
const computerTitleElement = document.getElementById("computer-title");
const computerDescriptionElement = document.getElementById(
  "computer-description"
);
const imageElement = document.getElementById("computer-image");

let balance = 0;
let computers = [];
let salary = 0;
let loan = 0;
let specs = [];

// Function that handles loan.
// Function checks if you try to input strings, if the prompt is empty -
// and if loan amount is not more than 2 X balance.
const handleLoan = () => {
  if (loan > 0) {
    alert("You can only ask for one loan!");
  } else {
    let askedLoan = prompt(
      "How much do you want to loan? (You can only ask for loan 2 X salary!)"
    );
    askedLoan = parseInt(askedLoan);

    if (askedLoan > balance * 2 || isNaN(askedLoan)) {
      alert("You can't do that");
    } else {
      balance += askedLoan;
      loan += askedLoan;
      loanElement.innerText = `Loan ${askedLoan} Kr`;
      document.getElementById("repay-loan-button").style.display = "";
    }
  }
  balanceElement.innerText = balance;
};
loanButtonElement.addEventListener("click", handleLoan);

// Function  that adds 100 to salary per button click.
function work() {
  salaryElement.innerHTML = salary += 100;
}

// Function that adds salary to balance.
// Function takes 10% of salary to repay loan if you have one.
// Function takes the rest (90% of salary if you have loan) and sends it to balance.
const addSalaryToBalance = () => {
  if (loan > 0) {
    let myLoan = salary * 0.1;
    let mySalary = salary * 0.9;

    if (myLoan > loan) {
      let rest = myLoan - loan;
      loan = 0;
      balance += rest + mySalary;
      balanceElement.innerText = balance + " kr";
      salary = 0;
      salaryElement.innerText = salary;
      loanElement.innerText = loan;
      document.getElementById("repay-loan-button").style.display = "none";
    } else {
      balance += mySalary;
      loan -= myLoan;
      balanceElement.innerText = balance;
      salary = 0;
      salaryElement.innerText = salary;
      loanElement.innerText = loan;
    }
  } else {
    balance += salary;
    balanceElement.innerText = balance;
    salary = 0;
    salaryElement.innerText = salary;
  }
};
saveSalaryElement.addEventListener("click", addSalaryToBalance);
salaryElement.innerHTML = salary;

// Function that handles loan.
// Button to repay loan only visible if you have loan.
const repayLoan = () => {
  let rest;
  if (loan > 0) {
    if (salary >= loan) {
      rest = salary - loan;
      balance += rest;
      salary = 0;
      loan = 0;
      rest = 0;
      salaryElement.innerHTML = salary;
      balanceElement.innerHTML = balance;
      loanElement.innerHTML = `Loan ${loan} Kr`;
      document.getElementById("repay-loan-button").style.display = "none";
    } else if (salary < loan) {
      rest = loan - salary;
      loan = rest;
      loanElement.innerHTML = `Loan ${loan} Kr`;
      salary = 0;
      salaryElement.innerHTML = salary;
    }
  }
};
repayLoanButtonElement.addEventListener("click", repayLoan);

// Fetch gets API call from link.
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

  // Function that adds computers from API call to array.
  // Prints computer: specs, description, title, image and price.
const addComputersToMenu = (computers) => {
  computers.forEach((computer) => addComputerToMenu(computer));
  const image = document.getElementById("computer-image");

  computerDescriptionElement.innerText = computers[0].description;
  computerSpecsElement.innerText = computers[0].specs;
  computerPriceElement.innerText = computers[0].price;
  computerTitleElement.innerText = computers[0].title;

  image.setAttribute(
    `src`,
    `https://noroff-komputer-store-api.herokuapp.com/${computers[0].image}`
  );
};

// Function that lets user choose computer from dropdown.
const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
};

// Function that handles change from dropdown and display the new computer values.
const handleComputerMenuChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  const image = imageElement;

  computerTitleElement.innerText = selectedComputer.title;
  computerDescriptionElement.innerText = selectedComputer.description;
  computerPriceElement.innerText = selectedComputer.price;
  computerSpecsElement.innerText = selectedComputer.specs;

  image.setAttribute(
    "src",
    `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`
  );

  priceElement.innerText = selectedComputer.price;
  computerSpecsElement.innerText = selectedComputer.specs.join("\n");
  computerDescription.innerText = selectedComputer.description;
};
computersElement.addEventListener("change", handleComputerMenuChange);

// Function that handles pay.
// Function checks if user has sufficient founds to buy computer, if not. User is not allowed to buy computer.
const handlePay = () => {
  const selectedComputer = computers[computersElement.selectedIndex];
  const computerTotal = selectedComputer.price;
  if (balance >= computerTotal) {
    balance = balance - computerTotal;
    balanceElement.innerHTML = `Balance ${balance}`;
    alert("You have purchased a computer");
  } else {
    alert("Not sufficient founds");
  }
};
buyButtonElement.addEventListener("click", handlePay);
